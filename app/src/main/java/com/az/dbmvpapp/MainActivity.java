package com.az.dbmvpapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import com.az.dbmvpapp.entities.myEntity;
import com.az.dbmvpapp.entities.repository;
import com.az.dbmvpapp.models.Item;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity implements DataAdapter.DataAdapterListener{
    private RecyclerView recyclerView;
private DataAdapter dataAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, EditItemActivity.class);
                intent.putExtra("ID",0);
                startActivityForResult(intent,0);
            }
        });


        loadData();


    }

    private void loadData() {
        repository.Get(getApplicationContext()).entityDao().getAll()
               //.flatMapIterable(list ->list)
                //.map(item -> new Item(item))
      //          .toList()

                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
.subscribe(new Consumer<List<myEntity>>() {
    @Override
    public void accept(List<myEntity> myEntities) throws Exception {
        List<Item> items = new ArrayList<>();
        for(myEntity e : myEntities){
            items.add(new Item(e));
        }
        onLoad(items);
    }
});
//        .subscribe(new Consumer<List<Item>>() {
//            @Override
//            public void accept(List<Item> myEntities) throws Exception {
//                onLoad(myEntities);
//            }
//        });
    }

    private void initRecyclerView(List<Item> items)
    {
        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        dataAdapter = new DataAdapter(items, this);
        recyclerView.setAdapter(dataAdapter);

        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
    }
    private  void onLoad(List<Item> myEntities){
        initRecyclerView(myEntities);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(Item item) {
        Intent intent = new Intent(MainActivity.this, EditItemActivity.class);
        intent.putExtra("ID",item.Id);
        startActivityForResult(intent,0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode ==0)
            loadData();
    }
}
