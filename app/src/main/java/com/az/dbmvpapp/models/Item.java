package com.az.dbmvpapp.models;

import android.databinding.BaseObservable;

import com.az.dbmvpapp.R;
import com.az.dbmvpapp.entities.myEntity;

public class Item  {
    public String Name;
    public String Description;
    public int Id;
    public Item(myEntity entity){
        Id = entity.id;
        Name = entity.Name;
        Description = entity.Description;
    }
    public Item(){

    }


}
