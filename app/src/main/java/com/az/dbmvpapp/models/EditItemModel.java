package com.az.dbmvpapp.models;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.ObservableField;
import android.view.View;

import com.az.dbmvpapp.entities.EntityDb;
import com.az.dbmvpapp.entities.myEntity;
import com.az.dbmvpapp.entities.repository;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;


public class EditItemModel {
    EntityDb db;
    Activity context;
    public EditItemModel(EntityDb db,Activity context){

        this.db = db;
        DeleteBtnVisible = new ObservableField<>();
        Progressbarvisible = new ObservableField<>();
        this.context = context;
    }

    public void onEditClick(Item item){
        Progressbarvisible.set(View.VISIBLE);
        Action action;
        if (item.Id ==0){
             action  = () -> db.entityDao()
                    .insertEntity(new myEntity(item));

          }
          else {
             action  = () -> db.entityDao()
                    .updateEntity(new myEntity(item));

        }
        Completable.fromAction(action)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onComplete);
    }

    public void onDeleteClick(Item item){
        Completable.fromAction(()->db.entityDao().deleteEntity(new myEntity(item)))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onComplete);
    }

    public ObservableField<Integer> DeleteBtnVisible;
    public ObservableField<Integer> Progressbarvisible;


   public Action onComplete = new Action() {
       @Override
       public void run() throws Exception {
           Progressbarvisible.set(View.GONE);
           Intent returnIntent = new Intent();

          context.setResult(context.RESULT_OK,returnIntent);

           context.finish();
       }
   };
}
