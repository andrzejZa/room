package com.az.dbmvpapp;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;


import com.az.dbmvpapp.databinding.ContentEditItemBinding;
import com.az.dbmvpapp.entities.EntityDb;
import com.az.dbmvpapp.entities.myEntity;
import com.az.dbmvpapp.entities.repository;
import com.az.dbmvpapp.models.EditItemModel;
import com.az.dbmvpapp.models.Item;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;


public class EditItemActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_item);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Item item = new Item();
        item.Id = getIntent().getIntExtra("ID",0);
        EntityDb db = repository.Get(this);
        EditItemModel model = new EditItemModel(db,this);
        ContentEditItemBinding binging = DataBindingUtil.setContentView(this, R.layout.content_edit_item);

        if (item.Id>0) {
            model.DeleteBtnVisible.set(View.VISIBLE);
            model.Progressbarvisible.set(View.VISIBLE);
            db.entityDao().getById(item.Id)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<myEntity>() {
                        @Override
                        public void accept(myEntity entity) throws Exception {
                            item.Name = (entity.Name);
                            item.Description = (entity.Description);
                            model.Progressbarvisible.set(View.GONE);
                           binging.setItem(item);
                        }
                    });
        }
        else
            model.DeleteBtnVisible.set(View.GONE);

        binging.setModel(model);
        binging.setItem(item);
    }

}
