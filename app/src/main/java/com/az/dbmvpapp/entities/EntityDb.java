package com.az.dbmvpapp.entities;

import android.arch.persistence.room.RoomDatabase;

@android.arch.persistence.room.Database(entities = {myEntity.class}, version = 1)
    public abstract class EntityDb extends RoomDatabase {
        public abstract EntityDao entityDao();
    }

