package com.az.dbmvpapp.entities;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.Single;

@Dao
public interface EntityDao {
    @Query("SELECT * FROM myEntity")
    Flowable<List<myEntity>> getAll();

    @Query("SELECT * FROM myEntity WHERE Name = :name ")
    Maybe<myEntity> findEntity(String name);

    @Query("SELECT * FROM myEntity  WHERE id = :id")
    Single<myEntity> getById(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertEntity(myEntity entity);

    @Update
    void updateEntity(myEntity entity);

    @Delete
    void deleteEntity(myEntity entity);

    @Query("DELETE FROM myEntity")
    void deleteAllEntities();
}
