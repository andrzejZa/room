package com.az.dbmvpapp.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.az.dbmvpapp.models.Item;

@Entity
public class myEntity {
    @PrimaryKey(autoGenerate = true)
    public int id;
    public String Name;
    public String Description;

    public myEntity(String name, String description) {
        Name = name;
        Description = description;
    }

    public myEntity(Item item) {
        this.id = item.Id;
        this.Name = item.Name;
        this.Description = item.Description;
    }
    public myEntity(){

    }
}
