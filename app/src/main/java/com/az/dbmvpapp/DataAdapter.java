package com.az.dbmvpapp;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.az.dbmvpapp.databinding.DataRowItemBinding;
import com.az.dbmvpapp.models.Item;

import java.util.List;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.MyViewHolder> {

    private List<Item> items;
    private LayoutInflater layoutInflater;
    private DataAdapterListener listener;


    public DataAdapter(List<Item> items, DataAdapterListener listener) {
        this.items = items;
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        DataRowItemBinding binding =
                DataBindingUtil.inflate(layoutInflater, R.layout.data_row_item, parent, false);

        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.binding.setItem(items.get(position));
        holder.binding.rowitemx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onItemClick(items.get(position));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public interface DataAdapterListener {
        void onItemClick(Item item);
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {

        private final DataRowItemBinding binding;

        public MyViewHolder(final DataRowItemBinding itemBinding) {
            super(itemBinding.getRoot());
            this.binding = itemBinding;
        }
    }


}
